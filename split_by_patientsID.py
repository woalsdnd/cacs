import pandas as pd
import numpy as np
import os

df = pd.read_csv("../data/CACS_label.csv")
unique_patients = np.unique(df["person"])
unique_patients = sorted(unique_patients)
N_patients = len(unique_patients)

if not os.path.exists("split_lists_feat"):
    os.mkdir("split_lists_feat")
# with open(os.path.join("../data/img_file_list"), "r") as f:
#     img_fnames = f.readlines()
# img_fnames = [img_fname.rstrip() for img_fname in img_fnames]
img_fnames = os.listdir("../data/fovea_occluded")
img_fnames = sorted(img_fnames)
patientID = np.array([int(img_fname.split("_")[0]) for img_fname in img_fnames])

for i in range(5):
    img_fpaths = []
    filepath = os.path.join("split_lists_feat", "chunk_{}".format(i))
    patients_chunk = unique_patients[i * N_patients // 5:(i + 1) * N_patients // 5]
    
    for patient_id in patients_chunk:
        for index in np.where(patientID == patient_id)[0]:
            img_fpaths.append(img_fnames[index])
    
    with open(filepath, "w") as f:
        for line in img_fpaths:
            f.write("../data/imgs/" + line + "\n")
