import xgboost as xgb
import pickle
import numpy as np
from sklearn.metrics import confusion_matrix
from sklearn.metrics import roc_auc_score

# load features, labels
with open("features_train.npy", 'rb') as f:
    train_X=pickle.load(f)
with open("labels_train.npy", 'rb') as f:
    train_Y=pickle.load(f)
with open("features_test.npy", 'rb') as f:
    val_X=pickle.load(f)
with open("labels_test.npy", 'rb') as f:
    val_Y=pickle.load(f)

# run xgboost
dtrain = xgb.DMatrix(train_X,label=train_Y)
dtest = xgb.DMatrix(val_X)
param = {'max_depth':7, 'eta':0.3, 'tree_method':"exact", 'min_child_weight':0.1 , 'silent':1, 'objective':'binary:logistic' }
num_round = 200
bst = xgb.train(param, dtrain, num_round)

# check training phase
preds = bst.predict(dtrain)
pred_labels=np.clip(np.round(preds), 0, 1)
cm=confusion_matrix(train_Y, pred_labels)
spe=1.*cm[0,0]/(cm[0,1]+cm[0,0]) if cm[0,1]+cm[0,0] !=0 else 0 
sen=1.*cm[1,1]/(cm[1,0]+cm[1,1]) if cm[1,1]+cm[1,0] !=0 else 0
acc=1.*(cm[0,0]+cm[1,1])/np.sum(cm)
print cm
print "specificity : {}, sensitivity : {}, accuracy : {}".format(spe,sen,acc)
print "auc roc : {}".format(roc_auc_score(train_Y, preds))
    
# make prediction
preds = bst.predict(dtest)
pred_labels=np.clip(np.round(preds), 0, 1)
cm=confusion_matrix(val_Y, pred_labels)
spe=1.*cm[0,0]/(cm[0,1]+cm[0,0]) if cm[0,1]+cm[0,0] !=0 else 0 
sen=1.*cm[1,1]/(cm[1,0]+cm[1,1]) if cm[1,1]+cm[1,0] !=0 else 0
acc=1.*(cm[0,0]+cm[1,1])/np.sum(cm)
print cm
print "specificity : {}, sensitivity : {}, accuracy : {}".format(spe,sen,acc)
print "auc roc : {}".format(roc_auc_score(val_Y, preds))