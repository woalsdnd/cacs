for threshold in 0 50 100 150 200 250 300 350 400;do
	python inference_final.py --threshold=${threshold} --batch_size=1 --gpu_index=2 --val_index=1 > ../results/inference/1_${threshold}
done

for threshold in 0 50 100 150 200 250 300 350 400;do
	python inference_final.py --threshold=${threshold} --batch_size=1 --gpu_index=2 --val_index=4 > ../results/inference/4_${threshold}
done