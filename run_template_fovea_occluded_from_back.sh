input_type=fovea_occluded
gpu_index=0
for i in 400; do
	for index_exp in 4 3; do
		python train_${input_type}.py --batch_size=32 --gpu_index=${gpu_index} --task_type=binary_${i} --index_exp=${index_exp} --class_weight=same > ../results/${index_exp}/threshold_exp/out_binary_${input_type}_${i};
	done;
done