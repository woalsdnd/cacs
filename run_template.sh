#input_type=fovea_occluded
input_type=vessels_occluded
#input_type=fundus_only
gpu_index=2
for i in 0 100 200 300 400; do
	for index_exp in 0 1 2 3 4; do
		python train_${input_type}.py --batch_size=32 --gpu_index=${gpu_index} --task_type=binary_${i} --index_exp=${index_exp} --class_weight=same > ../results/${index_exp}/threshold_exp/out_binary_${input_type}_${i};
	done;
done


#for i in 50 150 250 350; do
#	python train_fovea_occluded.py --batch_size=32 --gpu_index=0 --task_type=binary_${i} --index_exp=0 --class_weight=same > ../results/0/threshold_exp/out_binary_fovea_occluded_${i};
#done