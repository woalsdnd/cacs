import utils
import os
import argparse
import time
from keras import backend as K
import sys
import iterator
import models
import numpy as np

# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--batch_size',
    type=int,
    help="size of a batch for each GPU",
    required=True
    )
parser.add_argument(
    '--gpu_index',
    type=str,
    required=True
    )
parser.add_argument(
    '--val_index',
    type=int,
    required=True
    )
parser.add_argument(
    '--threshold',
    type=int,
    required=True
    )
FLAGS, _ = parser.parse_known_args()

# set gpu 
os.environ['CUDA_VISIBLE_DEVICES'] = FLAGS.gpu_index

# set misc settings
task_type = "binary_{}".format(FLAGS.threshold)
img_dir = "../data/imgs"
vessel_dir = "../data/vessels"
model_dir = os.path.join("../models_ori_network_resblock_modified", "binary_0", str(FLAGS.val_index))
image_format = ".tiff"
label_file = "../data/CACS_label.csv"

# training settings
train_files, validation_files = utils.set_files_for_exp("split_lists_feat", FLAGS.val_index)
validation_batch_fetcher = iterator.ValidationBatchFetcher(validation_files, vessel_dir, image_format, label_file, task_type, FLAGS.batch_size)
    
# load network
network = utils.load_network(model_dir)
    
# evaluate on validation set
pred_labels, true_labels = [], []
activations = []
filenames = []
start_time = time.time()
for batch_fns, batch_fundus, batch_vessel, batch_age, batch_htn, batch_dm, batch_sex, batch_y in validation_batch_fetcher():
    preds, activation = network.predict(batch_fundus)
    pred_labels += preds.tolist()
    true_labels += batch_y.tolist()
    activations.append(activation)
    filenames += batch_fns.tolist()

activations = np.concatenate(activations, axis=0)
utils.print_stats(true_labels, pred_labels)
# utils.analyze_imgs(filenames, true_labels, pred_labels, activations, activation_dir)
duration = time.time() - start_time
print "duration : {}".format(duration)
