import utils
import os
import argparse
import time
from keras import backend as K
import sys
import iterator
import models
import numpy as np

# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--batch_size',
    type=int,
    help="size of a batch for each GPU",
    required=True
    )
parser.add_argument(
    '--gpu_index',
    type=str,
    required=True
    )
parser.add_argument(
    '--task_type',
    type=str,
    help="task type",
    required=True
    )
FLAGS, _ = parser.parse_known_args()

assert "binary" in FLAGS.task_type  

# set gpu 
os.environ['CUDA_VISIBLE_DEVICES'] = FLAGS.gpu_index

# set misc settings
img_dir = "../data/imgs"
vessel_dir = "../data/vessels"
activation_dir = os.path.join("../activation", FLAGS.task_type)
utils.mkdir_if_not_exist(activation_dir)
model_dir = os.path.join("../models", FLAGS.task_type)
image_format = ".tiff"
label_file = "../data/CACS_label.csv"

# training settings
train_files, validation_files = utils.set_files_for_exp("split_lists_feat", 0)
validation_batch_fetcher = iterator.ValidationBatchFetcher(validation_files, vessel_dir, image_format, label_file, FLAGS.task_type, FLAGS.batch_size)
    
# load network
network = utils.load_network(model_dir)
    
# evaluate on validation set
pred_labels, true_labels = [], []
activations = []
filenames = []
start_time = time.time()
for batch_fns, batch_fundus, batch_vessel, batch_age, batch_htn, batch_dm, batch_sex, batch_y in validation_batch_fetcher():
    preds, activation = network.predict(batch_fundus)
    pred_labels += preds.tolist()
    true_labels += batch_y.tolist()
    activations.append(activation)
    filenames += batch_fns.tolist()

activations = np.concatenate(activations, axis=0)
utils.print_stats(true_labels, pred_labels)
utils.analyze_imgs(filenames, true_labels, pred_labels, activations, activation_dir)
duration = time.time() - start_time
print "duration : {}".format(duration)
