for threshold in 0 50 100 150 200 250 300 350 400;do
	python inference_final.py --threshold=${threshold} --batch_size=1 --gpu_index=1 --val_index=0 > ../results/inference/0_${threshold}
done

for threshold in 0 50 100 150 200 250 300 350 400;do
	python inference_final.py --threshold=${threshold} --batch_size=1 --gpu_index=1 --val_index=3 > ../results/inference/3_${threshold}
done