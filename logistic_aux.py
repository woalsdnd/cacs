import utils
import argparse
import numpy as np
from sklearn.linear_model import LogisticRegression
import scipy.stats as st
import os
import json

# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--task_type',
    type=str,
    help="task type",
    required=True
    )
FLAGS, _ = parser.parse_known_args()
image_format = ".tiff"
label_file = "../data/CACS_label.csv"
out_path = "../results/logistic_regression/"

results_dict = {"age":{"spe":[], "sen":[], "AUC_ROC":[]}, "age_sex":{"spe":[], "sen":[], "AUC_ROC":[]},
                "age_sex_htn":{"spe":[], "sen":[], "AUC_ROC":[]}}
for index_exp in range(5):
    print "="*10
    print "="*10
    print "exp num {}".format(index_exp)

    # training settings
    train_files, validation_files = utils.set_files_for_exp("split_lists_feat", index_exp)
    train_files, train_aux_inputs, train_labels = utils.labels_for_files(train_files, image_format, label_file, FLAGS.task_type)
    validation_files, validation_aux_inputs, validation_labels = utils.labels_for_files(validation_files, image_format, label_file, FLAGS.task_type)
    train_aux_inputs = utils.tuple2arr_aux(train_aux_inputs)
    validation_aux_inputs = utils.tuple2arr_aux(validation_aux_inputs)
    train_labels = np.array(train_labels) 
    validation_labels = np.array(validation_labels) 
    n_normal_train = len(train_labels[train_labels == 0])
    n_abnormal_train = len(train_labels[train_labels == 1])
    sample_weight = np.array([1 if train_label == 0 else 1.*n_normal_train / n_abnormal_train for train_label in train_labels])

    # run LogisticRegression 
    logistic_regressor = LogisticRegression()
    logistic_regressor.fit(train_aux_inputs[:, :1], train_labels, sample_weight)
    y_pred = logistic_regressor.predict_proba(validation_aux_inputs[:, :1])[:, 1]
    spe, sen, AUC_ROC = utils.binary_stats(validation_labels, y_pred)
    results_dict["age"]["spe"].append(spe)
    results_dict["age"]["sen"].append(sen)
    results_dict["age"]["AUC_ROC"].append(AUC_ROC)
 
    logistic_regressor = LogisticRegression()
    logistic_regressor.fit(train_aux_inputs[:, :2], train_labels, sample_weight)
    y_pred = logistic_regressor.predict_proba(validation_aux_inputs[:, :2])[:, 1]
    spe, sen, AUC_ROC = utils.binary_stats(validation_labels, y_pred)
    results_dict["age_sex"]["spe"].append(spe)
    results_dict["age_sex"]["sen"].append(sen)
    results_dict["age_sex"]["AUC_ROC"].append(AUC_ROC)
    
    logistic_regressor = LogisticRegression()
    logistic_regressor.fit(train_aux_inputs, train_labels, sample_weight)
    y_pred = logistic_regressor.predict_proba(validation_aux_inputs)[:, 1]
    spe, sen, AUC_ROC = utils.binary_stats(validation_labels, y_pred)
    results_dict["age_sex_htn"]["spe"].append(spe)
    results_dict["age_sex_htn"]["sen"].append(sen)
    results_dict["age_sex_htn"]["AUC_ROC"].append(AUC_ROC)

with open(os.path.join(out_path, 'results_{}.json'.format(FLAGS.task_type)), 'w') as fp:
    json.dump(results_dict, fp)

for task, result_dict in results_dict.iteritems():
    print task
    for metric, val in result_dict.iteritems():
        lb, ub = st.t.interval(0.95, len(val) - 1, loc=np.mean(val), scale=st.sem(val))
        print "{}: {} ({}-{})".format(metric, val, lb, ub)
