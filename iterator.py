import multiprocessing
import threading
import Queue
from uuid import uuid4

import numpy as np
import SharedArray

import utils
import os

img_h, img_w = 512, 512


def load_shared(args):
    i, array_name_fundus, fundus_fname, array_name_vessel, vessel_fname, is_train = args
    array_fundus = SharedArray.attach(array_name_fundus)
    array_vessel = SharedArray.attach(array_name_vessel)
    img_fundus, img_vessel = utils.load_augmented(fundus_fname, vessel_fname, augment=is_train)
    
    array_fundus[i] = img_fundus
    array_vessel[i] = img_vessel


def balance_per_class_indices(y, weights):
    weights = np.array(weights, dtype=float)
    p = np.zeros(len(y))
    for i, weight in enumerate(weights):
        p[y == i] = weight
    return np.random.choice(np.arange(len(y)), size=len(y), replace=True,
                            p=np.array(p) / p.sum())


class BatchIterator(object):

    def __init__(self, batch_size):
        self.batch_size = batch_size

    def __call__(self, X1, X2, aux_inputs, y):
        self.X1, self.X2, self.aux_inputs, self.y = X1, X2, aux_inputs, y
        return self

    def __iter__(self):
        n_samples = self.X1.shape[0]
        bs = self.batch_size
        for i in range((n_samples + bs - 1) // bs):
            sl = slice(i * bs, (i + 1) * bs)
            X1 = self.X1[sl]
            X2 = self.X2[sl]
            aux_inputs = self.aux_inputs[sl]
            y = self.y[sl]
            yield self.transform(X1, X2, aux_inputs, y)


class QueueIterator(BatchIterator):

    def __iter__(self):
        queue = Queue.Queue(maxsize=20)
        end_marker = object()

        def producer():
            for filenames, fundus_img, vessel_img, age, htn, dm, sex, yb in super(QueueIterator, self).__iter__():
                queue.put((np.array(filenames), np.array(fundus_img), np.array(vessel_img), np.array(age), np.array(htn), np.array(dm), np.array(sex), np.array(yb)))
            queue.put(end_marker)

        thread = threading.Thread(target=producer)
        thread.daemon = True
        thread.start()

        item = queue.get()
        while item is not end_marker:
            yield item
            queue.task_done()
            item = queue.get()


class SharedIterator(QueueIterator):

    def __init__(self, *args):
        self.pool = multiprocessing.Pool()
        super(SharedIterator, self).__init__(*args)

    def transform(self, X1, X2, aux_inputs, y):
        shared_array_name_fundus = str(uuid4())
        shared_array_name_vessel = str(uuid4())
        try:
            shared_array_fundus = SharedArray.create(
                shared_array_name_fundus, [len(X1), img_h, img_w, 3], dtype=np.float32)
            shared_array_vessel = SharedArray.create(
                shared_array_name_vessel, [len(X2), img_h, img_w], dtype=np.float32)
                                        
            fundus, vessel = X1, X2
            args = []
            
            for i in range(len(fundus)):
                args.append((i, shared_array_name_fundus, fundus[i], shared_array_name_vessel, vessel[i], self.is_train))

            self.pool.map(load_shared, args)
            Xb_fundus = np.array(shared_array_fundus, dtype=np.float32)
            Xb_vessel = np.array(shared_array_vessel, dtype=np.float32)

        finally:
            SharedArray.delete(shared_array_name_fundus)
            SharedArray.delete(shared_array_name_vessel)
        
        age_arr = np.zeros([len(X2), len(aux_inputs[0, 0])])
        htn_arr = np.zeros([len(X2), len(aux_inputs[0, 1])])
        dm_arr = np.zeros([len(X2), len(aux_inputs[0, 2])])    
        sex_arr = np.zeros([len(X2), len(aux_inputs[0, 3])])    
        for i in range(len(X1)):
            age_arr[i, ...] = aux_inputs[i, 0]
            htn_arr[i, ...] = aux_inputs[i, 1]
            dm_arr[i, ...] = aux_inputs[i, 2]
            sex_arr[i, ...] = aux_inputs[i, 3]
            
        return X1, Xb_fundus, Xb_vessel, age_arr, htn_arr, dm_arr, sex_arr, y


class TrainBatchFetcher(SharedIterator):

    def __init__(self, train_files, vessel_dir, image_format, train_label_file, task_type, batch_size, class_weight="same", reduce_negative=False):
        train_files, train_aux_inputs, train_labels = utils.labels_for_files(train_files, image_format, train_label_file, task_type)
        if reduce_negative:
            pos_train_files = train_files[train_labels == 1]
            pos_train_aux_inputs = train_aux_inputs[train_labels == 1]
            pos_train_labels = train_labels[train_labels == 1]
            n_pos = len(pos_train_files)
            neg_train_files = train_files[train_labels == 0][:n_pos]
            neg_train_aux_inputs = train_aux_inputs[train_labels == 0][:n_pos]
            neg_train_labels = train_labels[train_labels == 0][:n_pos]
            train_files = np.concatenate([pos_train_files, neg_train_files])
            train_aux_inputs = np.concatenate([pos_train_aux_inputs, neg_train_aux_inputs])
            train_labels = np.concatenate([pos_train_labels, neg_train_labels]).astype(int)
        self.train_fundus = np.array(train_files)
        self.train_vessel = np.array(map(lambda x : os.path.join(vessel_dir, os.path.basename(x)), self.train_fundus))
        self.train_aux_inputs = np.array(train_aux_inputs)
        self.train_labels = np.array(train_labels)
        if class_weight == "same" or class_weight == "schedule":
            self.weight_ori = np.array(utils.class_weight(train_labels))
        elif class_weight == "data":
            self.weight_ori = np.array([0.5, 0.5])
        else:
            raise ValueError("FLAGS.class_weight should be data, same or schedule")
        print "class weight: {}".format(self.weight_ori)
        if class_weight == "same" or class_weight == "data":
            self.weight = self.weight_ori
        self.is_train = True
        super(TrainBatchFetcher, self).__init__(batch_size)

    def __call__(self):
        indices = balance_per_class_indices(self.train_labels, weights=self.weight)
        X1 = self.train_fundus[indices]
        X2 = self.train_vessel[indices]
        aux_inputs = self.train_aux_inputs[indices]
        y = self.train_labels[indices]
        return super(TrainBatchFetcher, self).__call__(X1, X2, aux_inputs, y)
    
    def update_weights(self, pos_ratio):
        self.weight = np.array([self.weight_ori[0], self.weight_ori[1] * pos_ratio])
        if (self.weight_ori != self.weight).any():
            print "current weight {}".format(self.weight)


class ValidationBatchFetcher(SharedIterator):

    def __init__(self, validation_files, vessel_dir, image_format, validation_label_file, task_type, batch_size):
        validation_files, validation_aux_inputs, validation_labels = utils.labels_for_files(validation_files, image_format, validation_label_file, task_type)
        self.validation_fundus = np.array(validation_files)
        self.validation_vessel = np.array(map(lambda x : os.path.join(vessel_dir, os.path.basename(x)), self.validation_fundus))
        self.validation_aux_inputs = np.array(validation_aux_inputs)
        self.validation_labels = np.array(validation_labels)
        self.is_train = False
        super(ValidationBatchFetcher, self).__init__(batch_size)

    def get_filenames(self):
        return self.validation_fundus
    
    def __call__(self):
        return super(ValidationBatchFetcher, self).__call__(self.validation_fundus, self.validation_vessel, self.validation_aux_inputs, self.validation_labels)

