for threshold in 0 50 100 150 200 250 300 350 400;do
	python inference_final.py --threshold=${threshold} --batch_size=1 --gpu_index=3 --val_index=2 > ../results/inference/2_${threshold}
done