import argparse
import numpy as np
from skimage.restoration import inpaint
from PIL import Image
import utils
import os
from skimage import data
from scipy.misc import imresize
import time
from scipy.ndimage.interpolation import zoom

RESCALE_FACTOR = 4
MASK_THRESHOLD = 50

# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--img_dir_path',
    type=str,
    required=True
    )
parser.add_argument(
    '--mask_dir_path',
    type=str,
    required=True
    )
parser.add_argument(
    '--out_dir_path',
    type=str,
    required=True
    )
FLAGS, _ = parser.parse_known_args()

fpaths = utils.all_files_under(FLAGS.img_dir_path)
utils.mkdir_if_not_exist(FLAGS.out_dir_path)
for index, fpath in enumerate(fpaths):
#     st = time.time()
    img = np.array(Image.open(fpath))
    ori_size = img.shape[:2]
    h_target, w_target = img.shape[0] // RESCALE_FACTOR, img.shape[1] // RESCALE_FACTOR
    fname = os.path.basename(fpath)
    mask_path = os.path.join(FLAGS.mask_dir_path, fname)
    mask = np.array(Image.open(mask_path))
    mask[mask <= MASK_THRESHOLD] = 0
    mask[mask > MASK_THRESHOLD] = 255
    img[mask == 255] = 0
 
#     # down-sample
    img_resized = imresize(img, (h_target, w_target), 'nearest')
    mask_resized = imresize(mask, (h_target, w_target), 'nearest')
    # inpaint
    mask_resized[mask_resized <= MASK_THRESHOLD] = 0
    mask_resized[mask_resized > MASK_THRESHOLD] = 1
    img_resized[mask_resized == 1] = 0
    image_result_resized = inpaint.inpaint_biharmonic(img_resized, mask_resized, multichannel=True)
    # upsample
    image_result_orisize = imresize(image_result_resized * 255, ori_size, 'bilinear')
    mask_orisize = zoom(mask_resized, RESCALE_FACTOR, order=0)
    # replace
    img[mask > MASK_THRESHOLD, :] = image_result_orisize[mask > MASK_THRESHOLD , :]
     
    Image.fromarray(img.astype(np.uint8)).save(os.path.join(FLAGS.out_dir_path, fname))
#     print "duration: {}".format(time.time() - st)

