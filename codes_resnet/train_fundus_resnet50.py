import utils
import os
import argparse
import time
from keras import backend as K
import sys
import iterator
import resnet50
import numpy as np

# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--gpu_index',
    type=str,
    required=True
    )
parser.add_argument(
    '--index_exp',
    type=int,
    required=True
    )
FLAGS, _ = parser.parse_known_args()

# set gpu 
os.environ['CUDA_VISIBLE_DEVICES'] = FLAGS.gpu_index

# set misc settings
n_epochs = 500
batch_size = 8
task_type = "binary_0"
class_weight = "data"
img_dir = "../data/imgs"
vessel_dir = "../data/vessels"
model_dir = os.path.join("../models_no_atrous", task_type) 
utils.mkdir_if_not_exist(model_dir)
image_format = ".tiff"
label_file = "../data/CACS_label.csv"

# training settings
train_files, validation_files = utils.set_files_for_exp("split_lists_feat", FLAGS.index_exp)
train_batch_fetcher = iterator.TrainBatchFetcher(train_files, vessel_dir, image_format, label_file, task_type, batch_size, class_weight)
validation_batch_fetcher = iterator.ValidationBatchFetcher(validation_files, vessel_dir, image_format, label_file, task_type, batch_size)
    
# create network
img_shape = (512, 512, 3)
network = resnet50.ResNet50(img_shape)
print "optimizer: {}, lr: {}".format(network.optimizer, network.optimizer.lr)
network.summary()
network_name = "network_fundus_no_atrous.json"
with open(os.path.join(model_dir, network_name), 'w') as f:
    f.write(network.to_json())

# train the network 
best_AUC_ROC = 0
for epoch in range(n_epochs):

    # train on train set
    start_time = time.time()
    training_loss = {"loss":[]}
    start_time = time.time()
    for filenames, batch_fundus, batch_vessel, batch_age, batch_htn, batch_dm, batch_sex, batch_y in train_batch_fetcher():
        assert len(filenames) == 8
        loss = network.train_on_batch(batch_fundus, batch_y)
        training_loss["loss"] += [loss] * len(filenames)
    print "train loss:{}".format(np.mean(training_loss["loss"]))
    # evaluate on validation set
    pred_labels, true_labels = [], []
    for filenames, batch_fundus, batch_vessel, batch_age, batch_htn, batch_dm, batch_sex, batch_y in validation_batch_fetcher():
        preds = network.predict(batch_fundus)
        pred_labels += preds.tolist()
        true_labels += batch_y.tolist()
        
    utils.print_stats(true_labels, pred_labels)
    spe, sen, AUC_ROC = utils.binary_stats(true_labels, pred_labels)
    duration = time.time() - start_time
    print "{}th epoch duration : {}".format(epoch, duration)
    sys.stdout.flush()
    
