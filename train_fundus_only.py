import utils
import os
import argparse
import time
from keras import backend as K
import sys
import iterator
import models
import numpy as np

# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--batch_size',
    type=int,
    help="size of a batch for each GPU",
    required=True
    )
parser.add_argument(
    '--gpu_index',
    type=str,
    required=True
    )
parser.add_argument(
    '--task_type',
    type=str,
    help="task type",
    required=True
    )
parser.add_argument(
    '--index_exp',
    type=int,
    required=True
    )
parser.add_argument(
    '--class_weight',
    type=str,
    required=True
    )
FLAGS, _ = parser.parse_known_args()

assert "binary" in FLAGS.task_type  

# set gpu 
os.environ['CUDA_VISIBLE_DEVICES'] = FLAGS.gpu_index

# set misc settings
n_epochs = 50
img_dir = "../data/imgs"
vessel_dir = "../data/vessels"
model_dir = os.path.join("../models_finding_network", FLAGS.task_type) 
utils.mkdir_if_not_exist(model_dir)
image_format = ".tiff"
label_file = "../data/CACS_label.csv"
input_check_dir = "../input_checks/fundus_only"
utils.mkdir_if_not_exist(input_check_dir)

# training settings
train_files, validation_files = utils.set_files_for_exp("split_lists_feat", FLAGS.index_exp)
print "train files: {}, val_file: {}".format(len(train_files), len(validation_files))
train_batch_fetcher = iterator.TrainBatchFetcher(train_files, vessel_dir, image_format, label_file, FLAGS.task_type, FLAGS.batch_size, FLAGS.class_weight)
validation_batch_fetcher = iterator.ValidationBatchFetcher(validation_files, vessel_dir, image_format, label_file, FLAGS.task_type, FLAGS.batch_size)
schedules = {'lr':{'0': 0.001, '30':0.0001, '40':0.00001}}
    
# create network
img_shape = (512, 512, 3)
network = models.network_activation_out(img_shape, 0)
print network.optimizer
network.summary()
network_name = "network_fundus_finding_network.json"
with open(os.path.join(model_dir, network_name), 'w') as f:
    f.write(network.to_json())

# train the network 
scheduler = utils.Scheduler(schedules, schedules['lr']['0'])
check_train_batch, check_val_batch = True, True
best_AUC_ROC = 0
for epoch in range(n_epochs):
    # update step sizes, learning rates
#     schedule = utils.read_json("schedule_adam.json")
#     scheduler.update_schedule_from_json(schedule)
    scheduler.update_steps(epoch)
    K.set_value(network.optimizer.lr, scheduler.get_lr())
    if FLAGS.class_weight == "data" or FLAGS.class_weight == "same":
        pass
    elif FLAGS.class_weight == "schedule":
        train_batch_fetcher.update_weights(scheduler.get_pos_ratio())
    else:
        raise ValueError("FLAGS.class_weight should be data, same or schedule")

    # train on train set
    training_loss = {"loss_activation":[], "loss_class":[], "acc_class":[]}
    start_time = time.time()
    for filenames, batch_fundus, batch_vessel, batch_age, batch_htn, batch_dm, batch_sex, batch_y in train_batch_fetcher():
        if check_train_batch:
            utils.plot_imgs(batch_fundus * 255, os.path.join(input_check_dir, "train", "imgs"))
            check_train_batch = False
        loss_total, loss_class, loss_activation, acc_class, acc_activation = network.train_on_batch(batch_fundus, [batch_y, np.expand_dims(batch_vessel[:, ::32, ::32], axis=3)])
        training_loss["loss_activation"] += [loss_activation] * len(filenames)
        training_loss["loss_class"] += [loss_class] * len(filenames)
        training_loss["acc_class"] += [acc_class] * len(filenames)
        
    # evaluate on validation set
    pred_labels, true_labels = [], []
    for filenames, batch_fundus, batch_vessel, batch_age, batch_htn, batch_dm, batch_sex, batch_y in validation_batch_fetcher():
        if check_val_batch:
            utils.plot_imgs(batch_fundus * 255, os.path.join(input_check_dir, "val", "imgs"))
            check_val_batch = False
        preds, activation = network.predict(batch_fundus)
        pred_labels += preds.tolist()
        true_labels += batch_y.tolist()
        
    utils.print_stats(true_labels, pred_labels)
    spe, sen, AUC_ROC = utils.binary_stats(true_labels, pred_labels)
    duration = time.time() - start_time
    print "{}th epoch duration : {}".format(epoch, duration)
    sys.stdout.flush()
    
    # save weights
    if AUC_ROC > best_AUC_ROC:
        best_AUC_ROC = AUC_ROC
        network.save_weights(os.path.join(model_dir, "network_fundus_weight_best_auroc.h5"))
