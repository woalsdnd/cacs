import os
import re

from PIL import Image, ImageEnhance, ImageFilter

import numpy as np
import pandas as pd
from sklearn.metrics import confusion_matrix
from sklearn.metrics.ranking import roc_auc_score, precision_recall_curve, auc
import random
from skimage.transform import warp, AffineTransform
from subprocess import Popen, PIPE
import math
from scipy.misc import imresize
from skimage import color
from keras import backend as K
from keras.models import model_from_json
import matplotlib.cm as cm
import json


def set_files_for_exp(list_dir, n):
    lists = all_files_under(list_dir)
    train_files = []
    for index, l in enumerate(lists):
        if index == n:
            val_files = [line.rstrip('\n') for line in open(l)]
        else:
            train_files += [line.rstrip('\n') for line in open(l)]
    return train_files, val_files


def read_json(path_json):
    with open(path_json, 'r') as f:
        info = json.load(f)
    return info


def load_network(dir_name, trainable=False):
    network_file = all_files_under(dir_name, extension=".json")
    weight_file = all_files_under(dir_name, extension=".h5")
    assert len(network_file) == 1 and len(weight_file) == 1
    with open(network_file[0], 'r') as f:
        network = model_from_json(f.read())
    network.load_weights(weight_file[0])
    network.trainable = trainable
    for l in network.layers:
        l.trainable = trainable
    return network


def intermediate_out_func(network, list_input_layer_names, list_output_layer_names):
    list_inputs, list_outputs = [], []
    for input_layer_name in list_input_layer_names:
        list_inputs.append(network.get_layer(input_layer_name).input)
    for output_layer_name in list_output_layer_names:
        list_outputs.append(network.get_layer(output_layer_name).output)
    return K.function(list_inputs, list_outputs)


def clip_lambda(delta_la, delta_lc, min, max):
    if delta_la > 0 and delta_lc > 0:
        return np.clip(1.*delta_lc / delta_la, min, max)
    elif delta_la > 0 and delta_lc < 0:
        return min
    elif delta_la < 0 and delta_lc > 0:
        return max
    else:
        return 1


def mkdir_if_not_exist(out_dir):
    if not os.path.isdir(out_dir):
        os.makedirs(out_dir)


def overlap_activation(img, activation):
    h_target, w_target, _ = img.shape
    heatmap = (activation - np.min(activation)) / (np.max(activation) - np.min(activation))
    heatmap = imresize(heatmap, (h_target, w_target), 'bilinear')
    heatmap_pil_img = Image.fromarray(heatmap)
    heatmap_pil_img = heatmap_pil_img.filter(ImageFilter.GaussianBlur(32))
    heatmap_blurred = np.asarray(heatmap_pil_img)
    heatmap_blurred = np.uint8(cm.jet(heatmap_blurred)[..., :3] * 255)
    overlapped = (img * 0.7 + heatmap_blurred * 0.3).astype(np.uint8)
    return overlapped


def analyze_imgs(filenames, true_labels, pred_labels, activations, out_dir):
    assert len(true_labels) == len(pred_labels) and len(filenames) == len(true_labels)
    dir_FN = os.path.join(out_dir, "FN")
    dir_FP = os.path.join(out_dir, "FP")
    dir_TP = os.path.join(out_dir, "TP")
    dir_FN_activation = os.path.join(out_dir, "FN_activation_overlap")
    dir_FP_activation = os.path.join(out_dir, "FP_activation_overlap")
    dir_TP_activation = os.path.join(out_dir, "TP_activation_overlap")
    mkdir_if_not_exist(dir_FN)
    mkdir_if_not_exist(dir_FP)
    mkdir_if_not_exist(dir_TP)
    mkdir_if_not_exist(dir_FN_activation)
    mkdir_if_not_exist(dir_FP_activation)
    mkdir_if_not_exist(dir_TP_activation)
    pred_labels = outputs2labels(pred_labels, 0, 1)

    for i in range(len(filenames)):
        if not (true_labels[i] == 0 and pred_labels[i] == 0):
            if true_labels[i] == 1 and pred_labels[i] == 0:  # FN
                img_dir = dir_FN
                activation_dir = dir_FN_activation
            elif true_labels[i] == 0 and pred_labels[i] == 1:  # FP
                img_dir = dir_FP
                activation_dir = dir_FP_activation
            elif true_labels[i] == 1 and pred_labels[i] == 1:  # TP        
                img_dir = dir_TP
                activation_dir = dir_TP_activation
            
            pipes = Popen(["cp", filenames[i], os.path.join(img_dir, os.path.basename(filenames[i]))], stdout=PIPE, stderr=PIPE)
            std_out, std_err = pipes.communicate()

            activation_overlapped = overlap_activation(np.array(Image.open(filenames[i])), activations[i, :, :, 0])
            Image.fromarray(activation_overlapped).save(os.path.join(activation_dir, os.path.basename(filenames[i])))
            

def all_files_under(path, extension=None, append_path=True, sort=True):
    if append_path:
        if extension is None:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path)]
        else:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path) if fname.endswith(extension)]
    else:
        if extension is None:
            filenames = [os.path.basename(fname) for fname in os.listdir(path)]
        else:
            filenames = [os.path.basename(fname) for fname in os.listdir(path) if fname.endswith(extension)]
    
    if sort:
        filenames = sorted(filenames)
    
    return filenames


def imagefiles2arrs(filenames):
    img_shape = image_shape(filenames[0])
    images_arr = np.zeros((len(filenames), img_shape[0], img_shape[1], img_shape[2]), dtype=np.float32)
    
    for file_index in xrange(len(filenames)):
        images_arr[file_index] = np.array(Image.open(filenames[file_index]))
    
    return images_arr


def normalize(imgs):
    images_arr = 1.*imgs / 255.0
    return images_arr


def load_augmented(fundus_fname, vessel_fname, augment):
    # filenames, lms : list of arrays
    fundus_img = np.array(Image.open(fundus_fname))
    vessel_img = np.array(Image.open(vessel_fname))
    
    # remove low values 
    fundus_img[fundus_img < 10] = 0

    if augment:
        # random color, contrast, brightness perturbation
        fundus_img = random_perturbation(fundus_img)
        
        # random flip
        if random.getrandbits(1):
            fundus_img = fundus_img[:, ::-1, :]  # flip an image
            vessel_img = vessel_img[:, ::-1]
       
        # affine transform (scale, rotation)
        shift_y, shift_x = np.array(fundus_img.shape[:2]) / 2.
        shift_to_ori = AffineTransform(translation=[-shift_x, -shift_y])
        shift_to_img_center = AffineTransform(translation=[shift_x, shift_y])
        r_angle = random.randint(0, 359)
        scale_factor = 1.15
        scale = random.uniform(1. / scale_factor, scale_factor)
        tform = AffineTransform(scale=(scale, scale), rotation=np.deg2rad(r_angle))
        fundus_img = warp(fundus_img, (shift_to_ori + (tform + shift_to_img_center)).inverse, output_shape=(fundus_img.shape[0], fundus_img.shape[1]))
        vessel_img = warp(vessel_img, (shift_to_ori + (tform + shift_to_img_center)).inverse, output_shape=(vessel_img.shape[0], vessel_img.shape[1]))

    else:
        fundus_img = fundus_img.astype(np.float32) / 255.
        vessel_img = vessel_img.astype(np.float32) / 255.

    return fundus_img, vessel_img


def val2cat(vals, n_cat):
    tensor = np.zeros((vals.shape[0], n_cat))
    for index, v in enumerate(vals):
        tensor[index, int(v)] = 1
    return tensor


def class_weight(list_label):
    bin_count = np.bincount(list_label)
    weight = 1. / np.array(bin_count)
    weight /= sum(weight)
    print "data number"
    print bin_count
    return weight


def class_weight_category(label_file, label_type):
    df_train = pd.read_csv(label_file)
    df_train = df_train[df_train[label_type] != -1]
    data_ratio = df_train[label_type].value_counts(normalize=True)
    weight = 1. / data_ratio
    weight /= sum(weight)
    print "class weight : {}".format(weight)
    return weight


def image_shape(filename):
    img = Image.open(filename)
    img_arr = np.asarray(img)
    img_shape = img_arr.shape
    return img_shape


def dist(pt1, pt2):
    return np.sqrt((pt2[0] - pt1[0]) ** 2 + (pt2[1] - pt1[1]) ** 2)


def out_circle(pt, c_pt, r):
    return dist(pt, c_pt) > r


def random_perturbation(img):
    rescale_factor = 1.2
    im = Image.fromarray(img.astype(np.uint8))
    en_color = ImageEnhance.Color(im)
    im = en_color.enhance(random.uniform(1. / rescale_factor, rescale_factor))
    en_cont = ImageEnhance.Contrast(im)
    im = en_cont.enhance(random.uniform(1. / rescale_factor, rescale_factor))
    en_bright = ImageEnhance.Brightness(im)
    im = en_bright.enhance(random.uniform(1. / rescale_factor, rescale_factor))
    return np.asarray(im)


def print_metrics(itr, **kargs):
    print "*** Round {}  ====> ".format(itr),
    for name, value in kargs.items():
        print ("{} : {}, ".format(name, value)),
    print ""

 
def load_labels(label_file, label_type):
    df = pd.read_csv(label_file)
    if label_type == 'abnormal':
        return dict(zip(df.filename, df.abnormal))
    elif label_type == 'ungradable':
        return dict(zip(df.filename, df.ungradable))                    
    else:
        raise Exception("Unknown label type : {}".format(label_type))


def outputs2labels(outputs, min_val, max_val):
    return np.clip(np.round(outputs), min_val, max_val)

    
def check_labels(filepaths, labels, label_type):
    label_dict = load_labels("labels_train.csv", label_type)
    for index, filepath in enumerate(filepaths):  
        label = label_dict[re.sub(".tiff" + "$", "", os.path.basename(filepath))]
        assert label == labels[index]


def onehot(arr, n_labels):
    """
    >>> utils.onehot(np.array([0,1]), 2)
        array([[ 1.,  0.],
               [ 0.,  1.]])
    """
    n = len(arr)
    onehotcoded = np.zeros((n, n_labels))
    for val in range(n_labels):
        onehotcoded[..., val][arr == val] = 1
    
    return onehotcoded


def print_stats(y_true, y_pred, task_type="binary"):
    if task_type == "binary":
        cm = confusion_matrix(y_true, outputs2labels(y_pred, 0, 1))
        spe = 1.*cm[0, 0] / (cm[0, 1] + cm[0, 0])
        sen = 1.*cm[1, 1] / (cm[1, 0] + cm[1, 1])
        acc = 1.*np.trace(cm) / np.sum(cm)
        HM = 2 * spe * sen / (spe + sen)
        AUC_ROC = roc_auc_score(y_true, y_pred)
        precision, recall, _ = precision_recall_curve(y_true, y_pred)
        AUC_PR = auc(recall, precision)
        print cm
        print "spe : {},  sen : {}, acc : {}, harmonic mean : {}, AUC_PR : {},  ROC_AUC : {} ".format(spe, sen, acc, HM, AUC_PR, AUC_ROC)
    else:
        cm = confusion_matrix(y_true, outputs2labels(y_pred, 0, 3))
        acc = 1.*(cm[0, 0] + cm[1, 1] + cm[2, 2] + cm[3, 3]) / np.sum(cm)
        print cm
        print "accuracy : {}".format(acc)


def binary_stats(y_true, y_pred):
    cm = confusion_matrix(y_true, outputs2labels(y_pred, 0, 1))
    spe = 1.*cm[0, 0] / (cm[0, 1] + cm[0, 0])
    sen = 1.*cm[1, 1] / (cm[1, 0] + cm[1, 1])
    HM = 2 * spe * sen / (spe + sen)
    AUC_ROC = roc_auc_score(y_true, y_pred)
    return spe, sen, AUC_ROC


def get_metric(metrics, y_true, y_pred):
    values = []
    for metric in metrics:
        if metric == "HM":
            cm = confusion_matrix(y_true, outputs2labels(y_pred, 0, 1))
            spe = 1.*cm[0, 0] / (cm[0, 1] + cm[0, 0])
            sen = 1.*cm[1, 1] / (cm[1, 0] + cm[1, 1])
            HM = 2 * spe * sen / (spe + sen)
            values.append(HM)
        elif metric == "auroc":
            AUC_ROC = roc_auc_score(y_true, y_pred)
            values.append(AUC_ROC)

    return values


def plot_imgs(imgs, out_dir):
    if not os.path.isdir(out_dir):
        os.makedirs(out_dir)
    for i in range(imgs.shape[0]):
        Image.fromarray((imgs[i, ...]).astype(np.uint8)).save(os.path.join(out_dir, "imgs_{}.png".format(i + 1)))
        Image.fromarray(imgs[i, ::32, ::32].astype(np.uint8)).save(os.path.join(out_dir, "imgs_discrete_{}.png".format(i + 1)))
        resized_img = imresize(imgs[i, ...], (16, 16), 'bilinear')
        Image.fromarray(resized_img.astype(np.uint8)).save(os.path.join(out_dir, "imgs_resized_{}.png".format(i + 1)))

                
def print_stats_category(y_true, y_pred):
    cm = confusion_matrix(y_true, np.round(np.clip(y_pred, 0, 3))) 
    print cm

        
class Scheduler:

    def __init__(self, schedules, init_lr, pos_ratio=0):
        self.schedules = schedules
        self.lr = init_lr
        self.pos_ratio = pos_ratio

    def get_lr(self):
        return self.lr
    
#     def get_pos_ratio(self):
#         return self.pos_ratio
        
#     def update_schedule_from_json(self, schedule):
#         self.schedules = schedule
        
    def update_steps(self, n_round):
        key = str(n_round)
        if key in self.schedules['lr']:
            self.lr = self.schedules['lr'][key]
#         if key in self.schedules['pos_ratio']:
#             self.pos_ratio = self.schedules['pos_ratio'][key]


def range2category(val):
    if val == 0:
        return 0
    elif 0 < val and val <= 150:
        return 1
    elif 150 < val and val <= 400:
        return 2
    elif 400 < val:
        return 3


def age2category(val):
    one_hot = np.zeros(10)
    for i in range(10):
        if val < (i + 1) * 10:
            one_hot[i] = 1
            return one_hot


def disease2category(val):
    one_hot = np.zeros(3)
    if math.isnan(val):
        one_hot[2] = 1
    else:
        one_hot[int(val)] = 1
    return one_hot 


def disease2category_aux(val):
    if math.isnan(val) or val == 0:
        return np.zeros(1)
    else:
        return np.ones(1)


def sex2category(val):
    one_hot = np.zeros(2)
    if val == "F":
        one_hot[0] = 1
    else:
        one_hot[1] = 1
    return one_hot


def sex2category_aux(val):
    if val == "F":
        return np.zeros(1)
    else:
        return np.ones(1)


def load_labels(label_file, task_type):
    df = pd.read_csv(label_file)
    if "binary" in task_type:
        threshold = int(task_type.split("_")[1])
        df["class_neg"] = df["cacs"].map(lambda x:1 if x == 0 else 0)
        df["class_pos"] = df["cacs"].map(lambda x:1 if x > threshold else 0)
        df.loc[df["class_neg"] == 1, "class"] = 0
        df.loc[df["class_pos"] == 1, "class"] = 1
        df = df.dropna(subset=["class"])
    elif "category" in task_type:
        df["class"] = df["cacs"].map(range2category)
    
    df["age_cat"] = df["age"].map(age2category)   
    df["htn_cat"] = df["htn"].map(disease2category)
    df["dm_cat"] = df["dm"].map(disease2category)
    df["sex_cat"] = df["sex"].map(sex2category)
    
    return dict(zip(zip(df["person"], df["exam_date"]), df["class"])), dict(zip(zip(df["person"], df["exam_date"]), zip(df["age_cat"], df["htn_cat"], df["dm_cat"], df["sex_cat"]))) 


def load_labels_aux(label_file, task_type):
    df = pd.read_csv(label_file)
    if "binary" in task_type:
        threshold = int(task_type.split("_")[1])
        df["class_neg"] = df["cacs"].map(lambda x:1 if x == 0 else 0)
        df["class_pos"] = df["cacs"].map(lambda x:1 if x > threshold else 0)
        df.loc[df["class_neg"] == 1, "class"] = 0
        df.loc[df["class_pos"] == 1, "class"] = 1
        df = df.dropna(subset=["class"])
    elif "category" in task_type:
        df["class"] = df["cacs"].map(range2category)
    
    df["htn_cat"] = df["htn"].map(disease2category_aux)
    df["sex_cat"] = df["sex"].map(sex2category_aux)
    
    return dict(zip(zip(df["person"], df["exam_date"]), df["class"])), dict(zip(zip(df["person"], df["exam_date"]), zip(df["age"], df["htn_cat"], df["sex_cat"]))) 


def tuple2arr(list_tuples):
    dim = 0
    for i in range(len(list_tuples[0])):
        dim += len(list_tuples[0][i])
    arr = np.zeros((len(list_tuples), dim))
    
    for i in range(len(list_tuples)):
        start_index = 0
        for j in range(len(list_tuples[0])):
            arr[i, start_index:start_index + len(list_tuples[i][j])] = list_tuples[i][j]
            start_index += len(list_tuples[i][j])
    
    return arr


def tuple2arr_aux(list_tuples):
    return np.array(list_tuples)


def labels_for_files(filepaths, img_format, label_file, task_type):
    """
    exclude files not in the label
    """
    label_dict, aux_dict = load_labels_aux(label_file, task_type)
     
    n_missing = 0
    new_filepaths = []
    label_list = []
    aux_inputs = []
    list_not_fundus = ["2303108485_20090813_I00655443", "5509383054_20091130_I00800076", "5509383054_20091130_I00800077",
                     "5509383054_20091130_I00800078", "5509383054_20091130_I00800079", "5801836966_20090729_I00645171",
                     "5801836966_20090729_I00645172", "6559577747_20090828_I00681114", "7516136452_20090611_I00562748",
                     "8519131944_20061026_I00288885", "8519131944_20061026_I00288887", "8519131944_20061026_I00288888",
                     "8519131944_20061026_I00288891", "9312797571_20070723_I00242510", "9877547596_20090722_I00633498"]
    
    for filepath in filepaths:
        fname = os.path.basename(filepath)
        split_list = re.sub(img_format + "$", "", fname).split("_")
        identifier = (int(split_list[0]), int(split_list[1]))
        if identifier in label_dict and fname.split(".")[0] not in list_not_fundus:
            label_list.append(label_dict[identifier])
            aux_inputs.append(aux_dict[identifier])
            new_filepaths.append(filepath)
        else:
            n_missing += 1
             
    print "missing labels : {}".format(n_missing)
    return np.array(new_filepaths), np.array(aux_inputs), np.array(label_list).astype(int)

