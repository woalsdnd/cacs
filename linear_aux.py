import utils
import argparse
import numpy as np
from sklearn.linear_model import LinearRegression
import scipy.stats as st
import os
import json

# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--task_type',
    type=str,
    help="task type",
    required=True
    )
FLAGS, _ = parser.parse_known_args()
image_format = ".tiff"
label_file = "../data/CACS_label.csv"
out_path = "../results/linear_regression/"

results_dict = {"age":{"spe":[], "sen":[], "AUC_ROC":[]}, "age_sex":{"spe":[], "sen":[], "AUC_ROC":[]},
                "age_sex_htn":{"spe":[], "sen":[], "AUC_ROC":[]}}
for index_exp in range(5):
    print "="*10
    print "="*10
    print "exp num {}".format(index_exp)

    # training settings
    train_files, validation_files = utils.set_files_for_exp("split_lists_feat", index_exp)
    train_files, train_aux_inputs, train_labels = utils.labels_for_files(train_files, image_format, label_file, FLAGS.task_type)
    validation_files, validation_aux_inputs, validation_labels = utils.labels_for_files(validation_files, image_format, label_file, FLAGS.task_type)
    train_aux_inputs = utils.tuple2arr(train_aux_inputs)
    validation_aux_inputs = utils.tuple2arr(validation_aux_inputs)
    train_labels = np.array(train_labels) 
    validation_labels = np.array(validation_labels) 
    n_normal_train = len(train_labels[train_labels == 0])
    n_abnormal_train = len(train_labels[train_labels == 1])
    sample_weight = np.array([1. / n_normal_train if train_label == 0 else 1. / n_abnormal_train for train_label in train_labels])

    # run linear regressor
    linear_regressor = LinearRegression()
    linear_regressor.fit(train_aux_inputs[:, :10], train_labels, sample_weight)
    y_pred = linear_regressor.predict(validation_aux_inputs[:, :10])
    spe, sen, AUC_ROC = utils.binary_stats(validation_labels, y_pred)
    results_dict["age"]["spe"].append(spe)
    results_dict["age"]["sen"].append(sen)
    results_dict["age"]["AUC_ROC"].append(AUC_ROC)
     
    linear_regressor = LinearRegression()
    linear_regressor.fit(np.concatenate([train_aux_inputs[:, :10], train_aux_inputs[:, -2:]], axis=1), train_labels, sample_weight)
    y_pred = linear_regressor.predict(np.concatenate([validation_aux_inputs[:, :10], validation_aux_inputs[:, -2:]], axis=1))
    spe, sen, AUC_ROC = utils.binary_stats(validation_labels, y_pred)
    results_dict["age_sex"]["spe"].append(spe)
    results_dict["age_sex"]["sen"].append(sen)
    results_dict["age_sex"]["AUC_ROC"].append(AUC_ROC)
 
    train_tmp = np.copy(train_aux_inputs)
    validation_tmp = np.copy(validation_aux_inputs)
    train_tmp[train_aux_inputs[:, 12] == 1, 10:13] = np.array([1, 0, 0])
    validation_tmp[validation_aux_inputs[:, 12] == 1, 10:13] = np.array([1, 0, 0])
    linear_regressor = LinearRegression()
    n_normal_train = len(train_labels[train_labels == 0])
    n_abnormal_train = len(train_labels[train_labels == 1])
    sample_weight = np.array([1. / n_normal_train if train_label == 0 else 1. / n_abnormal_train for train_label in train_labels])
    input_train_age_sex_htn = np.concatenate([train_tmp[:, :13], train_tmp[:, -2:]], axis=1)
    input_val_age_sex_htn = np.concatenate([validation_tmp[:, :13], validation_tmp[:, -2:]], axis=1)
    linear_regressor.fit(input_train_age_sex_htn, train_labels, sample_weight)
    y_pred = linear_regressor.predict(input_val_age_sex_htn)
    spe, sen, AUC_ROC = utils.binary_stats(validation_labels, y_pred)
    results_dict["age_sex_htn"]["spe"].append(spe)
    results_dict["age_sex_htn"]["sen"].append(sen)
    results_dict["age_sex_htn"]["AUC_ROC"].append(AUC_ROC)
  
with open(os.path.join(out_path, 'results_{}.json'.format(FLAGS.task_type)), 'w') as fp:
    json.dump(results_dict, fp)
  
for task, result_dict in results_dict.iteritems():
    print task
    for metric, val in result_dict.iteritems():
        lb, ub = st.t.interval(0.95, len(val) - 1, loc=np.mean(val), scale=st.sem(val))
        print "{}: {} ({}-{})".format(metric, val, lb, ub)
